####################################
# Script create CSV SCIMago
####################################
# SCIMAGO et CORE sont des agences de notation de journaux et conferences scientifiques
# SCIMAGO est generaliste et donne des notes Q1, Q2, Q3 et Q4 et ne note que les journaux
# https://www.scimagojr.com/
# CORE est specialise en informatique et donne des notes A*, A, B et C et note journaux et conferences
# http://portal.core.edu.au/conf-ranks/
#
# Ce script permet la creation de CSV contenant pour les journaux le titre et la note, et pour les conferences, le titre, l'acronyme et la note
# Principe : 
# - on telecharge des CSV a partir d'URL dans des fichiers
# - on parse les CSV en recuperant les infos souhaitees (titre et note ou titre, acronyme et note) dans une ou des listes
# - on reconstruit un CSV a partir de la ou les listes
#
# Pour SCIMAGO, les journaux sont recuperes par annee pr tous les domaines
# Pour CORE, les conf sont recuperes par annee mais une seule base pour les journaux
####################################

import requests
import csv
import os


###########
# VARIABLES
###########

# URLs SCIMAGO pour tous les les domaines informatique
url_sci_2021 = 'https://www.scimagojr.com/journalrank.php?year=2021&type=j&out=xls'
url_sci_2020 = 'https://www.scimagojr.com/journalrank.php?year=2020&type=j&out=xls'
url_sci_2019 = 'https://www.scimagojr.com/journalrank.php?year=2019&type=j&out=xls'
url_sci_2018 = 'https://www.scimagojr.com/journalrank.php?year=2018&type=j&out=xls'
url_sci_2017 = 'https://www.scimagojr.com/journalrank.php?year=2017&type=j&out=xls'
url_sci_2016 = 'https://www.scimagojr.com/journalrank.php?year=2016&type=j&out=xls'
url_sci_2015 = 'https://www.scimagojr.com/journalrank.php?year=2015&type=j&out=xls'
url_sci_2014 = 'https://www.scimagojr.com/journalrank.php?year=2014&type=j&out=xls'
url_sci_2013 = 'https://www.scimagojr.com/journalrank.php?year=2013&type=j&out=xls'


# URLs CORE pour les conferences
url_core_conf_2021="http://portal.core.edu.au/conf-ranks/?search=&by=all&source=CORE2021&sort=arank&page=1&do=Export"
url_core_conf_2020="http://portal.core.edu.au/conf-ranks/?search=&by=all&source=CORE2020&sort=arank&page=1&do=Export"
url_core_conf_2018="http://portal.core.edu.au/conf-ranks/?search=&by=all&source=CORE2018&sort=arank&page=1&do=Export"
url_core_conf_2017="http://portal.core.edu.au/conf-ranks/?search=&by=all&source=CORE2017&sort=arank&page=1&do=Export"
url_core_conf_2014="http://portal.core.edu.au/conf-ranks/?search=&by=all&source=CORE2014&sort=arank&page=1&do=Export"
url_core_conf_2013="http://portal.core.edu.au/conf-ranks/?search=&by=all&source=CORE2013&sort=arank&page=1&do=Export"
#url_core_jnl_all="http://portal.core.edu.au/jnl-ranks/?search=&by=all&source=all&sort=atitle&page=1&do=Export"

# Fichiers CSV crees
new_csv_sci_21="ranking/scimago/SCIMago-J-2021.csv"
new_csv_sci_20="ranking/scimago/SCIMago-J-2020.csv"
new_csv_sci_19="ranking/scimago/SCIMago-J-2019.csv"
new_csv_sci_18="ranking/scimago/SCIMago-J-2018.csv"
new_csv_sci_17="ranking/scimago/SCIMago-J-2017.csv"
new_csv_sci_16="ranking/scimago/SCIMago-J-2016.csv"
new_csv_sci_15="ranking/scimago/SCIMago-J-2015.csv"
new_csv_sci_14="ranking/scimago/SCIMago-J-2014.csv"
new_csv_sci_13="ranking/scimago/SCIMago-J-2013.csv"

new_csv_core_c_21="ranking/core/CORE-C-2021.csv"
new_csv_core_c_20="ranking/core/CORE-C-2020.csv"
new_csv_core_c_18="ranking/core/CORE-C-2018.csv"
new_csv_core_c_17="ranking/core/CORE-C-2017.csv"
new_csv_core_c_14="ranking/core/CORE-C-2014.csv"
new_csv_core_c_13="ranking/core/CORE-C-2013.csv"

# Compteurs
cnt_scimago_jrn=0
cnt_core_conf=0
cnt_problems_sautligne=0

###########
# METHODS
###########

# Creation des fichiers SCIMago contenant titre des journaux et notes 
# Lecture 3 CSV telecharge de la meme annee envoyes en parametres 
# Enregistrement titre et note a partir des 3 CSV dans 3 listes : list_sci_com, list_sci_mat, list_sci_eng si note = Q1, Q2, Q3 ou Q4
# Ecriture d'un nouveau fichier csv a partir des 3 listes
def create_csv_scimago(csvsci, new_csv) :
    global cnt_scimago_jrn
    cnt_scimago_jrn+=1
    print ("begin create_csv_scimago {0}".format(new_csv[16:]))
    csv_sci_com = csv.reader(csvsci.splitlines(), delimiter=str(';'), quotechar='|')
    list_sci_com = []
    for row in list(csv_sci_com) :
        title = str(row[2])
        #print ("SJR TITLE = ",title)
        title = title.replace('"','')
        try :
            if (row[6] == "Q1") or (row[6] == "Q2") or (row[6] == "Q3") or (row[6] == "Q4") :
                list_sci_com.append((title,str(row[6])))
        except IndexError as inderr :
            print("Error for title ",title)

    with open(new_csv, 'w') as myfile:
        writefile = csv.writer(myfile, delimiter='|')
        for journal in list_sci_com :
            titre = journal[0]
            titre=titre.replace(',',' ')
            note = journal[1]
            writefile.writerow([titre, note])


# Creation des fichiers CORE conferences contenant titre des conf, acronymes et notes
# Lecture du CSV telecharge envoye en parametre 
# Enregistrement nom_conf, acronyme et note dans 1 liste : list_core si note = A*, A, B ou C
# Ecriture d'un nouveau fichier csv a partir de la liste
def create_csv_core_conf(csv_core_conf, new_csv) :
    global cnt_core_conf
    cnt_core_conf+=1
    print ("begin create_csv_core_conf {0}".format(new_csv[13:]))
    csv_core = csv.reader(csv_core_conf.splitlines(), delimiter=str(','), quotechar='"')
    list_core = []
    global cnt_problems_sautligne
    for row in csv_core :
        title = str(row[1])
        if "\n" in title :
            title=title.replace("\r\n","") 
            cnt_problems_sautligne+=1
        acronym = str(row[2])
        note = str(row[4])
        if (note == "A*") or (note == "A") or (note == "B") or (note == "C") :
            list_core.append((title,acronym,note))

    with open(new_csv, 'w') as myfile:
        writefile = csv.writer(myfile, delimiter='|')
        for conf in list_core :
            titre = conf[0]
            acro = conf[1]
            note = conf[2]
            writefile.writerow([titre, acro, note])

###########
# PROGRAMME
###########
def create_csv_scimago_core():
    '''
    Download CSV from Scimago and Core and for each, create a new CSV with formalism
    - For Scimago : 'name of journal' | note
    - For Core : 'name of conf' | 'acronym of conf' | note
    The created CSV are in /ranking/scimago/ and /ranking/core/

    return boolean boolscript : True if success, False otherwise
    '''

    bool_script = True
    try :
        # Download files from SCIMAGO and create 5 CSV
        file_sci_com_21 = requests.get(url_sci_2021)
        create_csv_scimago(file_sci_com_21.content.decode("utf-8"), new_csv_sci_21)

        file_sci_com_20 = requests.get(url_sci_2020)
        create_csv_scimago(file_sci_com_20.content.decode("utf-8"), new_csv_sci_20)

        file_sci_com_19 = requests.get(url_sci_2019)
        create_csv_scimago(file_sci_com_19.content.decode("utf-8"), new_csv_sci_19)

        file_sci_com_18 = requests.get(url_sci_2018)
        create_csv_scimago(file_sci_com_18.content.decode("utf-8"), new_csv_sci_18)

        file_sci_com_17 = requests.get(url_sci_2017)
        create_csv_scimago(file_sci_com_17.content.decode("utf-8"), new_csv_sci_17)

        file_sci_com_16 = requests.get(url_sci_2016)
        create_csv_scimago(file_sci_com_16.content.decode("utf-8"), new_csv_sci_16)

        file_sci_com_15 = requests.get(url_sci_2015)
        create_csv_scimago(file_sci_com_15.content.decode("utf-8"), new_csv_sci_15)

        file_sci_com_14 = requests.get(url_sci_2014)
        create_csv_scimago(file_sci_com_14.content.decode("utf-8"), new_csv_sci_14)

        file_sci_com_13 = requests.get(url_sci_2013)
        create_csv_scimago(file_sci_com_13.content.decode("utf-8"), new_csv_sci_13)

        # Download files from CORE ,create CSV 
        file_core_conf_21 = requests.get(url_core_conf_2021)
        create_csv_core_conf(file_core_conf_21.content.decode("utf-8"),new_csv_core_c_21)

        file_core_conf_20 = requests.get(url_core_conf_2020)
        create_csv_core_conf(file_core_conf_20.content.decode("utf-8"),new_csv_core_c_20)

        file_core_conf_18 = requests.get(url_core_conf_2018)
        create_csv_core_conf(file_core_conf_18.content.decode("utf-8"),new_csv_core_c_18)

        file_core_conf_17 = requests.get(url_core_conf_2017)
        create_csv_core_conf(file_core_conf_17.content.decode("utf-8"),new_csv_core_c_17)

        file_core_conf_14 = requests.get(url_core_conf_2014)
        create_csv_core_conf(file_core_conf_14.content.decode("utf-8"),new_csv_core_c_14)

        file_core_conf_13 = requests.get(url_core_conf_2013)
        create_csv_core_conf(file_core_conf_13.content.decode("utf-8"),new_csv_core_c_13)

        print ("")
        print ("RESULTATS CREATION CSV SCIMAGO CORE")
        print ("Fichiers CSV SCIMago : "+str(cnt_scimago_jrn))
        print ("Fichiers CSV CORE conf : "+str(cnt_core_conf))
        print ("problemes saut de lignes regles : "+str(cnt_problems_sautligne))
        print ("")
    
    except Exception as e :
        bool_script = False

    return bool_script

