import requests
import csv

from script_create_csv_scimago_core import create_csv_scimago_core

######## VARIABLES
name_etab ="LIMOS"
begin_search = 2010
end_search = 2020

### SCIMAGO Files
# Articles
S_J_csv2013 = "ranking/scimago/SCIMago-J-2013.csv"
S_J_csv2014 = "ranking/scimago/SCIMago-J-2014.csv"
S_J_csv2015 = "ranking/scimago/SCIMago-J-2015.csv"
S_J_csv2016 = "ranking/scimago/SCIMago-J-2016.csv"
S_J_csv2017 = "ranking/scimago/SCIMago-J-2017.csv"
S_J_csv2018 = "ranking/scimago/SCIMago-J-2018.csv"
S_J_csv2019 = "ranking/scimago/SCIMago-J-2019.csv"
S_J_csv2020 = "ranking/scimago/SCIMago-J-2020.csv"
S_J_csv2021 = "ranking/scimago/SCIMago-J-2021.csv"

### CORE Files
# Conf
C_C_csv2013 = "ranking/core/CORE-C-2013.csv"
C_C_csv2014 = "ranking/core/CORE-C-2014.csv"
C_C_csv2017 = "ranking/core/CORE-C-2017.csv"
C_C_csv2018 = "ranking/core/CORE-C-2018.csv"
C_C_csv2020 = "ranking/core/CORE-C-2020.csv"
C_C_csv2021 = "ranking/core/CORE-C-2021.csv"

#### Counters
problems_name_title=""

##########  GET LIST OF PUBLICATIONS
def get_publilist():
    '''
    Get user data from 'nom_collection.txt' and parse it to get name of collection, begin and end of research
    Format an URL with this data and send a request to HAL to get a list of publictaions
    Result of the request is a JSON file which is parsed to get some specific data of each publication 
    A new list is created with for each entry :
    - the halid
    - the url of publi in HAL
    - the title
    - the doctype according to HAL (ART, COMM, ...)
    - list of authors
    - date of publication
    - journal if exists
    - conf if exists

    Return the list of publication publilist
    '''
    global name_etab, begin_search, end_search
    try :
        with open('nom_collection.txt') as f:
            line = f.readlines()
            print("variable utilisateur : {0}".format(line[0]))
            #name_etab = line[0]
            words = line[0].split('|')
            name_etab = words[0]
            begin_search = words[1]
            end_search = words[2]

    except Exception as e :
        print("Impossible de lire le fichier variables.txt")

    try :
        print("Get publis from HAL")
        url_search_publis = "https://api.archives-ouvertes.fr/search/{0}/?q=*:*&start=0&rows=9900&sort=producedDate_s%20desc&fq=producedDate_s:[{1}%20TO%20{2}]&fl=uri_s,halId_s,authFullName_s,title_s,docType_s,producedDate_s,journalTitle_s,conferenceTitle_s".format(name_etab,begin_search,end_search)
        #url_search_publis = "https://api.archives-ouvertes.fr/search/{0}/?q=*:*&start=0&rows=9900&sort=producedDate_s%20desc&fl=uri_s,halId_s,authFullName_s,title_s,docType_s,producedDate_s,journalTitle_s,conferenceTitle_s".format(name_etab)

        req = requests.get(url_search_publis)
        print ("reponse HTTP : {0} - reponse headers : {1}".format(req.status_code,req.headers['content-type']))

        json = req.json()
        publilist = []

        # recup en json et on se place dans response puis docs
        tous_docs = json['response']['docs']

        # pour chaque element de 'docs', on recupere les elements de la publi que l'on place dans publilist
        cptpub=0
        for doc in tous_docs:
            #print "lecture reponse"
            cptpub+=1
            list_authors = []
            list_authIdHal = []

            #recup halId
            recup_halId = doc["halId_s"]
            halid = '\"{0}\"'.format(recup_halId)

            # recup url
            uri = doc["uri_s"]
            uri = '\"{0}\"'.format(uri)

            #recup doctype
            doctype = doc["docType_s"]
            doctype = '\"{0}\"'.format(doctype)

            #recup title
            title = doc["title_s"]
            title[0].encode("utf-8")
            title[0] = title[0].replace("\""," ")

            # recup authors
            try :
                tous_authors = doc["authFullName_s"]
                for auth in tous_authors:
                    list_authors.append((auth))
            except KeyError as e :
                list_authors = "" 

            # recup producedDate
            producedDate = doc["producedDate_s"]

            # recup journal
            # Un 'try' est necessaire car pas forcement rensigne dans HAL
            try :
                journal = doc["journalTitle_s"]	
                journal = journal.replace("\""," ")
            except Exception as e:
                journal = ""

            # recup conf
            # Un 'try' est necessaire car pas forcement rensigne dans HAL
            try :
                conf = doc["conferenceTitle_s"]	
                conf = conf.replace("\""," ")
            except Exception as e:
                conf = ""

            publilist.append((halid,uri,title[0],doctype,list_authors, producedDate, journal, conf, list_authIdHal))

        print("Nb de publis récupérées {0}".format(len(publilist)))
        #for pub in publilist :
        #    print("{0} {1} ".format(pub[0],pub[2]))
        #print(publilist[0])
    
    except Exception as e :
        publilist = None

    return publilist


############# METHODS NOTATIONS PUBLIS

###################
#### SCIMAGO ######
###################

# supprime le contenu des parentheses et les parentheses de la conf issue du CSV
def splitParenthese(nameConf):
    #global count_parenthese
    p1 = nameConf.find("(")
    p2 = nameConf.find(")")    
    nameWithoutParenthese = nameConf[:p1]+nameConf[p2+1:]
    #count_parenthese+=1
    return nameWithoutParenthese


# Met a jour la base avec une note Scimago ou Core pour une publi de type ARTICLE ayant un journal
# En entree, un CSV, le journal de la publi, l'id de la publi et l'organsime (soit 'not_scimago' soit 'not_core')
# - met le contenu du CSV (journal + note) dans une liste -> list_journal
# - pour chaque element de cette liste, si le journal du CSV correspond au journal de la publi -> maj
# - maj avec curUpdatePubJ puis incrementation de compteurs
def findJournalNote_by_year(csv, journalpub) :
    '''
    Take as argument : CSV from /ranking/scimago  and name of journal of the publication
    Data of CSV ,journal and associated note, are set in a list
    Name of publi journal is searched over this list and if match, note is returned

    Return a tuple (match_conf,note) with : 
    - match_conf as boolean : True if journal match and False otherwise
    - note as String
    '''
    global problems_name_title 
    match_conf = False
    note=""

    list_journal = []
    for row in csv:
        try :
            journal = row[0]
            note_journal = row[1]
            list_journal.append((journal,note_journal))
        except :
            problems_name_title = problems_name_title + row[0] + "\n"


    for journal in list_journal :
        journrank=journal[0]
        journalpub=journalpub.replace(',','')
        journrank = journrank.replace(',','')
        journalpub=journalpub.replace(':','')
        journrank = journrank.replace(':','')
        journalpub=journalpub.replace('.','')
        journrank = journrank.replace('.','')
        journalpub = journalpub.replace('&','and')
        journalpub=journalpub.replace('  ',' ')
        journrank = journrank.replace('  ',' ')
        journalpub=journalpub.strip()
        journrank = journrank.strip()

        if journrank.upper() == journalpub.upper() :
            note = journal[1]
            #print ("-> match, journal= ",journal[0],"note = ",note)
            match_conf=True

    match_plus_note=(match_conf,note)
    return match_plus_note

def getNoteSJR(publiJ) :
    '''
    From one publication, get date and name of journal
    According to the date, a CSV in /ranking/scimago/ is selected and the method findJournalNote_by_year is called with this CSV and the name of journal
    A note is returned  from the method findJournalNote_by_year and is then returned

    Return note (String)
    '''
    idpub = publiJ[0]
    journalpub = publiJ[6]
    datepub = publiJ[5]
    match_conf = False
    note=""

    # verif matching scimago with date publi
    if ("2010" in str(datepub)) or ("2011" in str(datepub)) or ("2012" in str(datepub)) or ("2013" in str(datepub)):
        csvscimago = csv.reader(open(S_J_csv2013,"r"), delimiter=str('|'))
        match_plus_note=findJournalNote_by_year(csvscimago,journalpub)
        match_conf = match_plus_note[0]
        note = match_plus_note[1]

    if (("2013" in str(datepub)) or ("2014" in str(datepub))) and (match_conf == False) :
        csvscimago = csv.reader(open(S_J_csv2014,"r"), delimiter=str('|'))
        match_plus_note=findJournalNote_by_year(csvscimago,journalpub)
        match_conf = match_plus_note[0]
        note = match_plus_note[1]

    if (("2014" in str(datepub)) or ("2015" in str(datepub))) and (match_conf == False) :
        csvscimago = csv.reader(open(S_J_csv2015,"r"), delimiter=str('|'))
        match_plus_note=findJournalNote_by_year(csvscimago,journalpub)
        match_conf = match_plus_note[0]
        note = match_plus_note[1]

    if (("2015" in str(datepub)) or ("2016" in str(datepub))) and (match_conf == False) :
        csvscimago = csv.reader(open(S_J_csv2016,"r"), delimiter=str('|'))
        match_plus_note=findJournalNote_by_year(csvscimago,journalpub)
        match_conf = match_plus_note[0]
        note = match_plus_note[1]

    if (("2016" in str(datepub)) or ("2017" in str(datepub))) and (match_conf == False) :
        csvscimago = csv.reader(open(S_J_csv2017,"r"), delimiter=str('|'))
        match_plus_note=findJournalNote_by_year(csvscimago,journalpub)
        match_conf = match_plus_note[0]
        note = match_plus_note[1]

    if (("2017" in str(datepub)) or ("2018" in str(datepub))) and (match_conf == False) :
        csvscimago = csv.reader(open(S_J_csv2018,"r"), delimiter=str('|'))
        match_plus_note=findJournalNote_by_year(csvscimago,journalpub)
        match_conf = match_plus_note[0]
        note = match_plus_note[1]

    if (("2018" in str(datepub)) or ("2019" in str(datepub))) and (match_conf == False) :
        csvscimago = csv.reader(open(S_J_csv2019,"r"), delimiter=str('|'))
        match_plus_note=findJournalNote_by_year(csvscimago,journalpub)
        match_conf = match_plus_note[0]
        note = match_plus_note[1]

    if (("2019" in str(datepub)) or ("2020" in str(datepub))) and (match_conf == False) :
        csvscimago = csv.reader(open(S_J_csv2020,"r"), delimiter=str('|'))
        match_plus_note=findJournalNote_by_year(csvscimago,journalpub)
        match_conf = match_plus_note[0]
        note = match_plus_note[1]

    if (("2020" in str(datepub)) or ("2021" in str(datepub)) or ("2022" in str(datepub))) and (match_conf == False) :
        csvscimago = csv.reader(open(S_J_csv2021,"r"), delimiter=str('|'))
        match_plus_note=findJournalNote_by_year(csvscimago,journalpub)
        match_conf = match_plus_note[0]
        note = match_plus_note[1]

    return note


###################
###### CORE #######
###################

def findConfNote_by_year(csv, confpub, acroBool) :
    '''
    Take as argument : CSV from /ranking/core , name of conference of the publication and boolean which indicates if the acronym must be searched
    Data of CSV ,conference, acronym and associated note, are set in a list
    If acrobool is False, name of publi conference is searched over the column conference in list and if match, note is returned
    If acrobool is True, name of publi conference is searched over the column acronym in list and if match, note is returned (only if acronym length>3)

    Return a tuple (match_conf_core,note) with : 
    - match_conf_core as boolean : True if conf match and False otherwise
    - note as String
    '''
    global  problems_name_title
    note=""
    list_conf = []
    match_conf_core = False

    for row in csv:
        try :
            conf = row[0]#.decode("utf-8")
            if conf.find("(") > 0 :
                conf = splitParenthese(conf)
            acronym = row[1]
            note_conf = row[2]
            #print ("conf ",conf," - acronym ",acronym, " - note",note)
            list_conf.append((conf,acronym,note_conf))
        except IndexError :
            problems_name_title = problems_name_title + row[0] + "\n"

    if acroBool == False :
        for conf in list_conf :
            confrank=conf[0]
            confpub=confpub.replace(',','')
            confrank = confrank.replace(',','')
            confpub=confpub.replace(':','')
            confrank = confrank.replace(':','')
            confpub=confpub.replace('.','')
            confrank = confrank.replace('.','')
            confpub=confpub.replace('  ',' ')
            confrank = confrank.replace('  ',' ')
            confpub=confpub.strip()
            confrank = confrank.strip()

            if confrank.upper() in confpub.upper() :
                note = conf[2]
                #print ("-> match, conf= ",conf[0],"note= ",note)
                match_conf_core = True

    if acroBool == True :
        for conf in list_conf :
            acronymconf = conf[1]
            if (len(acronymconf) > 3) and (acronymconf.upper()==acronymconf):
                acronymconftest1=" "+acronymconf
                acronymconftest2="("+acronymconf
                acronymconftest3=acronymconf+" "
                acronymconftest4=acronymconf+"'"
                acronymconftest5=acronymconf+")"
                if (acronymconftest1 in confpub) or (acronymconftest2 in confpub)  or (acronymconftest3 in confpub)  or (acronymconftest4 in confpub) or (acronymconftest5 in confpub) :
                    note=conf[2]
                    #print ("-> match, conf (acronym)= ",conf[1]," - conf=",confpub,"note= ",note)
                    match_conf_core = True

            elif len(acronymconf) > 3 :
                acronymconf = acronymconf.upper()
                acronymconftest1=" "+acronymconf
                acronymconftest2="("+acronymconf
                acronymconftest3=acronymconf+" "
                acronymconftest4=acronymconf+"'"
                acronymconftest5=acronymconf+")"
                if (acronymconftest1 in confpub) or (acronymconftest2 in confpub)  or (acronymconftest3 in confpub)  or (acronymconftest4 in confpub) or (acronymconftest5 in confpub) :
                    note = conf[2]
                    #print ("-> match, conf (acronym)= ",conf[1]," - conf=",confpub,"note= ",note)
                    match_conf_core = True 

    match_plus_note=(match_conf_core,note)
    return  match_plus_note


def getNoteCore(publiC) :
    '''
    From one publication, get date and name of conference
    According to the date, a CSV in /ranking/core/ is selected and the method findConfNote_by_year is called with this CSV and the name of conference
    A note is returned  from the method findConfNote_by_year and is then returned

    Return note (String)
    '''
    idpub = publiC[0]
    confpub = publiC[7]
    datepub = publiC[5]
    match_conf_core = False
    note=""

    # verif matching core exact conference with date publi
    if ("2010" in str(datepub)) or ("2011" in str(datepub)) or ("2012" in str(datepub)) or ("2013" in str(datepub)):
        csvCcore = csv.reader(open(C_C_csv2013,"r"), delimiter=str('|'))
        match_plus_note = findConfNote_by_year(csvCcore, confpub, False)
        match_conf_core = match_plus_note[0]
        note = match_plus_note[1]

    if  (("2014" in str(datepub)) or ("2015" in str(datepub))) and (match_conf_core == False) :
        csvCcore = csv.reader(open(C_C_csv2014,"r"), delimiter=str('|'))
        match_plus_note = findConfNote_by_year(csvCcore, confpub, False)
        match_conf_core = match_plus_note[0]
        note = match_plus_note[1]

    if (("2016" in str(datepub)) or ("2017" in str(datepub))) and (match_conf_core == False)  :
        csvCcore = csv.reader(open(C_C_csv2017,"r"), delimiter=str('|'))
        match_plus_note = findConfNote_by_year(csvCcore, confpub, False)
        match_conf_core = match_plus_note[0]
        note = match_plus_note[1]

    if (("2018" in str(datepub)) or ("2019" in str(datepub))) and (match_conf_core == False)   :
        csvCcore = csv.reader(open(C_C_csv2018,"r"), delimiter=str('|'))
        match_plus_note = findConfNote_by_year(csvCcore, confpub, False)
        match_conf_core = match_plus_note[0]
        note = match_plus_note[1]

    if (("2019" in str(datepub)) or ("2020" in str(datepub))) and (match_conf_core == False)  :
        csvCcore = csv.reader(open(C_C_csv2020,"r"), delimiter=str('|'))
        match_plus_note = findConfNote_by_year(csvCcore, confpub, False)
        match_conf_core = match_plus_note[0]
        note = match_plus_note[1]

    if (("2021" in str(datepub)) or ("2022" in str(datepub))) and (match_conf_core == False)  :
        csvCcore = csv.reader(open(C_C_csv2021,"r"), delimiter=str('|'))
        match_plus_note = findConfNote_by_year(csvCcore, confpub, False)
        match_conf_core = match_plus_note[0]
        note = match_plus_note[1]

    # verif matching core acronym with date publi
    if (("2010" in str(datepub)) or ("2011" in str(datepub)) or ("2012" in str(datepub)) or ("2013" in str(datepub))) and (match_conf_core == False) :
        csvCcore = csv.reader(open(C_C_csv2013,"r"), delimiter=str('|'))
        match_plus_note = findConfNote_by_year(csvCcore, confpub, True)
        match_conf_core = match_plus_note[0]
        note = match_plus_note[1]

    if  (("2014" in str(datepub)) or ("2015" in str(datepub))) and (match_conf_core == False) :
        csvCcore = csv.reader(open(C_C_csv2014,"r"), delimiter=str('|'))
        match_plus_note = findConfNote_by_year(csvCcore, confpub, True)
        match_conf_core = match_plus_note[0]
        note = match_plus_note[1]

    if (("2016" in str(datepub)) or ("2017" in str(datepub))) and (match_conf_core == False)  :
        csvCcore = csv.reader(open(C_C_csv2017,"r"), delimiter=str('|'))
        match_plus_note = findConfNote_by_year(csvCcore, confpub, True)
        match_conf_core = match_plus_note[0]
        note = match_plus_note[1]

    if (("2018" in str(datepub))  or ("2019" in str(datepub))) and (match_conf_core == False)  :
        csvCcore = csv.reader(open(C_C_csv2018,"r"), delimiter=str('|'))
        match_plus_note = findConfNote_by_year(csvCcore, confpub, True)
        match_conf_core = match_plus_note[0]
        note = match_plus_note[1]

    if (("2019" in str(datepub)) or ("2020" in str(datepub))) and (match_conf_core == False)  :
        csvCcore = csv.reader(open(C_C_csv2020,"r"), delimiter=str('|'))
        match_plus_note = findConfNote_by_year(csvCcore, confpub, True)
        match_conf_core = match_plus_note[0]
        note = match_plus_note[1]

    if (("2021" in str(datepub)) or ("2022" in str(datepub))) and (match_conf_core == False)  :
        csvCcore = csv.reader(open(C_C_csv2021,"r"), delimiter=str('|'))
        match_plus_note = findConfNote_by_year(csvCcore, confpub, True)
        match_conf_core = match_plus_note[0]
        note = match_plus_note[1]

    #print("id :{0} - date {1} - note {2}".format(idpub,datepub,note))
    return note


######################
##### PROGRAMME  #####
######################

scriptbool = create_csv_scimago_core()

if scriptbool :
    publist = get_publilist()

    if publist != None :
        noteS_Q1 = 0
        noteS_Q2 = 0
        noteS_Q3 = 0
        noteS_Q4 = 0
        otherS = ""

        noteC_Astar = 0
        noteC_A = 0
        noteC_B = 0
        noteC_C = 0
        otherC = ""

        with open('publis_notes.csv', 'w', newline='') as csvfile:
            csvwriter = csv.writer(csvfile, delimiter=',',
                                    quotechar='"', quoting=csv.QUOTE_NONNUMERIC)

            for pub in publist :
                if pub[6] != "" : # si un journal est indique pr la publi
                    notation = getNoteSJR(pub)
                    newcsvrow = [pub[0],pub[2],pub[3],pub[4],pub[5],pub[6],pub[7],notation]
                    if notation=='Q1' :
                        noteS_Q1+=1
                    elif notation == 'Q2' :
                        noteS_Q2+=1
                    elif notation == 'Q3' :
                        noteS_Q3+=1
                    elif notation == 'Q4' :
                        noteS_Q4+=1


                elif pub[7] != "" : # si une conf est indique pr la publi
                    notation = getNoteCore(pub)
                    newcsvrow = [pub[0],pub[2],pub[3],pub[4],pub[5],pub[6],pub[7],notation]
                    if notation == 'A*':
                        noteC_Astar+=1
                    elif notation == 'A' :
                        noteC_A+=1
                    elif notation == 'B' :
                        noteC_B+=1
                    elif notation == 'C' :
                        noteC_C+=1

                else :
                    newcsvrow = [pub[0],pub[2],pub[3],pub[4],pub[5],pub[6],pub[7],""]
                
                csvwriter.writerow(newcsvrow)

        with open('synthese.txt', 'w') as txtfile:
            txtfile.write("SYNTHES DES RESULTATS POUR LE LABO {0} de {1} à {2}\n".format(name_etab, begin_search, end_search))
            txtfile.write("-----------------------------------------------------------\n")
            txtfile.write("Nombre de publications listées: {0}\n".format(len(publist)))
            txtfile.write("\n")
            txtfile.write("Résultats SCIMAGO\n")
            txtfile.write("Nombre de Q1 : {0} - Nombre de Q2 : {1} - Nombre de Q3 : {2} - Nombre de Q4 : {3}\n".format(noteS_Q1,noteS_Q2,noteS_Q3,noteS_Q4))
            txtfile.write("\n")
            txtfile.write("Résultats CORE\n")
            txtfile.write("Nombre de A* : {0} - Nombre de A : {1} - Nombre de B : {2} - Nombre de C : {3}\n".format(noteC_Astar,noteC_A,noteC_B,noteC_C))


        print("Problèmes dans le nom des journaux et confs des CSV : {0}".format(problems_name_title))
        print()
        print("Le script s'est déroulé correctement")
        print("Le fichier de résultat est 'publis_notes.csv'")
        print("Le fichier de synthèse est 'synthese.txt'")

    else :
        print("Problèmes à la création de la liste des publis")

else :
    print("Problèmes à la création des CSV Scimago et Core")