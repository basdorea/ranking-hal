# RANKING-HAL

## Présentation et explications

### Présentation

Ce script permet de récupérer les publications d'une [collection HAL](https://doc.archives-ouvertes.fr/gerer-une-collection/) et d'attribuer une note aux publications qui la composent à partir d'organismes de ranking

- [ScimagoJR](https://www.scimagojr.com/), généraliste, ranke les articles
- [CORE](http://portal.core.edu.au/conf-ranks/), domaine informatique, ranke les conférences

Le ranking se faisant par association entre les données de ces organismes et les données de HAL, la qualité des données de la collection influera fortement sur le résultat.

Le nom des journaux où ont été publiés les articles ainsi que les noms des conférences (ou leur acronyme) sont utilisés pour cette association et sont donc particulièrement importants.

### Explications

les fichiers de ranking de Scimago et Core n'étant valables que pour les années entre 2010 et 2022, seules les publications entre ces deux dates peuvent être notées même si les publications peuvent être téléchargées depuis une date antérieure à 2010.

Pour les journaux l'association se fait entre le nom du jouranl indiqué par Scimago et le nom du journal rentré dans HAL. Pour un meilleur matching, un traitement à base de suppression de caractères inutile (virgules, point, doubles espaces, ...) et de mise en majuscule est appliqué des deux côtés

Pour les conférences l'association se fait entre le nom de la conférence indiqué par Core et le nom de la conférence rentrée dans HAL. Pour un meilleur matching, le même traitement est appliqué que pour les journaux.

Puis, toujours pour les conférences, si aucun résultat n'a été trouvé à partir du nom exact de la conf, une tentative de matching est faite à partir de l'acronyme de la conférence et de la conf rentrée dans HAL (à condition que cet acronyme ait plus de 3 caractères)

L'idée étant de maximiser le matching tout en limitant le nombre de faux positifs.

Des premiers scripts permettant le ranking des publis ont été réalisés en 2019 pour un usage interne avec l'aide d'un chercheur pour affiner la méthode, puis a été remis à jour en 2022.

Les notes des journaux et confs évoluant au fil des années, la date de la publication est prise en compte pour choisir le fichier CSV de Scimago ou Core correspondant à cette date

## Déploiement et utilisation

### Déploiement

Ce script est écrit en python 3 et nécessite une librairie `requests` ; il est préférable de le lancer dans un environnement virtuel

Téléchargez le repo Gitlab `ranking-hal` et rentrez dans le répertoire

```bash
git clone https://gitlab.limos.fr/basdorea/ranking-hal.git
cd ranking-hal
```

Créez un environnement virtuel et activez le

```bash
sudo apt-get install python-pip  # si non fait
pip install --user virtualenv    # si non fait
mkdir venv
virtualenv -p python3 venv

source venv/bin/activate
```

Téléchargez la librairie

```bash
pip install requests
```

### Utilisation

Le déploiement doit avoir été fait.

Editez le fichier `nom_collection.txt` en indiquant :

- le nom de la collection (sans espaces avant ou après, ni quotes), c'est en général le nom du labo en majuscule
- la date de début de la recherche
- la date de fin de la recherche

Le fichier final doit ressembler à cale `LIMOS|2010|2022`

Lancer la commande :

`python note_publis_hal.py`

Le résultat est un fichier CSV `publis_notes.csv` se trouvant dans le répertoire courant ; il est possible de l'ouvrir avec un tableur.

**Attention** le fichier CSV doit s'ouvrir avec comme séparateur la virgule `,` et comme délimiteur de chîne de caractère le double quote `"`

Un fichier de synthèse , `synthese.txt` ,se trouve aussi dans le répertoire courant.

## Déroulé du script

Le script principal est nommé `note_publis_hal.py`, il fait appel à un autre script `script_create_csv_scimago_core.py`

Dans une **première étape**, le script principal appelle la méthode `create_csv_scimago_core` du script `script_create_csv_scimago_core.py`

Pour chaque entrée indiquée en haut de page du script, les données des organismes de ranking sont téléchargées, mises en fortme et enregistrées dans des fichiers CSV dans les répertoires `/ranking/scimago/` et `/ranking/core/`. Les entrées de cs CSV sont sous la forme `<nom_article_ou_conf>|note`

Dans une **seconde étape**, le script principal appelle la méthode `get_publilist`. Le fichier à remplir par l'utilisateur, `variables.txt` est lu pour connaître le nom de la collection à chercher et les dates de début et de fin de la recherche

Une URL est construite à partir de ces données et une requête est envoyée sur HAL pour récupérer les publications de la collection

Le résultat de la requête en JSON est récupéré puis parsé afin d'établir une liste comprenant pour chaque entrée :

- id Hal de la publi
- Titre de la publi
- Type de publi (formalisme HAL) , soit "ART", "COMM", "THESE",  ...
- Auteurs
- Date de publication
- Nom du journal (si il existe)
- Nom de la conférence (si elle existe)
- Note de la publication (champ vide au début qui sera rempli par la suite)

Dans une **troisième étape**, le script utilise la liste précédente et pour chaque ligne (chaque entrée) remplit un CSV, `publis_notes.csv`

Si le champ `journal` est rempli, il fait appel à la méthode `getNoteSJR` qui renverra une note pour la publi, si le champ `conférence` est rempli, il fait appel à la méthode `getNoteCore`, si les deux champs sont vides, le champ note est laissé vide

Ainsi, à la fin, le fichier `publis_notes.csv` est généré avec pour certaines publis de la collection, une note venant de Core ou Scimago

Un autre fichier `synthese.txt` donne une synthèse du déroulé du script avec le nombre d'occurences (càd de publications associées) pour chaque note.

La méthode `getNoteSJR` cherchera la date de la publi et prendra en compte le bon CSV Scimago pour appeler la fonction `findJournalNote_by_year` qui renverra une note

La méthode `getNoteCore` cherchera la date de la publi et prendra en compte le bon CSV Core pour appeler la fonction `findConfNote_by_year` qui renverra une note

## Autres outils HAL

Pour des infos supplémentaires sur la façon dont nous avons utilisé HAL au labo, vouis pouvez lire ce poste de blog : [https://perso.limos.fr/~basdorea/outils_hal.html](https://perso.limos.fr/~basdorea/outils_hal.html)

Un repo montant une interface (Python / Django) permettant d'importer massivement dans HAL à partir de fichiers texte ou de données DBLP : [https://gitlab.limos.fr/basdorea/haltools/](https://gitlab.limos.fr/basdorea/haltools/)
